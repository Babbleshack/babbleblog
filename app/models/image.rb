class Image < ActiveRecord::Base
  validates :path, presence: true
end
